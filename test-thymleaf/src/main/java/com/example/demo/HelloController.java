package com.example.demo;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
	
	//URL指定
	@RequestMapping("/")
	public String index(Model model) {
		//引数のモデルに対して値をセットする引数は２つで変数名と出力内容
		model.addAttribute("msg","Hello Thymleaf");
		//出力先のファイル名
	    return "index";
	}
	
	
	@RequestMapping("/home")
	public String test(Model tt) {
		//引数のモデルに対して値をセットする引数は２つで変数名と出力内容
		tt.addAttribute("tt","テスト出力");
		//出力先のファイル名
	    return "home";
	}
	
	
}
